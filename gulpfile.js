var gulp = require('gulp');
var jade = require('gulp-jade');
var sass = require('gulp-sass');
var sassGlob = require('gulp-sass-glob');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var image = require('gulp-imagemin');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

var dest = "build/";

// gulp.task('bootstrap', function() {
//     gulp.src('source/bootstrap/scss/bootstrap.scss')
//         .pipe(sourcemaps.init())
//         .pipe(sass())
//         .pipe(autoprefixer({
//             cascade: false
//         }))
//         .pipe(sourcemaps.write())
//         .pipe(gulp.dest('source/bootstrap/dist/css'));
// });

gulp.task('html', function() {
    return gulp.src('source/jade/*.jade')
        .pipe(jade({ pretty: true }))
        .pipe(gulp.dest(dest))
        .pipe(reload({ stream: true }));
});

gulp.task('css', function() {
    gulp.src('source/sass/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sassGlob())
        .pipe(sass({
            outputStyle: 'expanded'
        }).on('error', sass.logError))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(dest + 'css'))
        .pipe(reload({ stream: true }));
});

gulp.task('js', function() {
    return gulp.src(['source/js/*.js'])
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(dest + 'js'))
        .pipe(reload({ stream: true }));
});

gulp.task('vendorJs', function() {
    return gulp.src(['source/libs/**/*.js'])
        .pipe(concat('vendor.min.js'))
        .pipe(gulp.dest(dest + 'js'))
        .pipe(reload({ stream: true }));
});

gulp.task('vendorCss', function() {
    return gulp.src(['source/libs/**/*.css'])
        .pipe(concat('vendor.min.css'))
        .pipe(gulp.dest(dest + 'css'))
        .pipe(reload({ stream: true }));
});

gulp.task('image', function() {
    gulp.src('source/img/**/*')
        .pipe(image({
            progressive: true,
            interlaced: true,
            verbose: true,
        }))
        .pipe(gulp.dest(dest + 'img'));
});

gulp.task('copyAssets', function() {
    gulp.src('source/fonts/*.{ttf,woff,eof,svg}').pipe(gulp.dest(dest + 'fonts'));
});

gulp.task('watch', function() {
    browserSync.init({
        server: dest,
        port: 8191
    });
    gulp.watch(['source/jade/**/*'], ['html']);
    gulp.watch(['source/sass/**/*'], ['css']);
    gulp.watch(['source/js/**/*'], ['js']);
    // gulp.watch(['source/img/**/*'], ['image']);
});

gulp.task('default', ['html', 'css', 'js', 'vendorJs', 'vendorCss', 'image', 'copyAssets', 'watch']);
gulp.task('start', ['html', 'css', 'js', 'watch']);