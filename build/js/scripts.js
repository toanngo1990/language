// COMMON
(function() {
    // Select all
    var buttonSelectAll = $("[data-select]");
    var buttonDeselect = $("[data-deselect]");

    buttonSelectAll.on("click", function() {
        $(buttonSelectAll.data("select")).find("input[type='checkbox']").prop("checked", true);
    });

    buttonDeselect.on("click", function() {
        $(buttonDeselect.data("deselect")).find("input[type='checkbox']").prop("checked", false);
    });

    // Drag and drop
    $(".list-chips .chip").draggable({
        // revert: true,
        helper: "clone",
        start: function( event, ui ) {
            $(this).addClass("active");
        },
        stop: function( event, ui ) {
            $(this).removeClass("active");
        },
    });

    $(".list-paragraph .chip").droppable({
        accept: ".chip",
        // drop: function( event, ui ) {
        //   $( this )
        //     .addClass( "ui-state-highlight" )
        //     .find( "p" )
        //       .html( "Dropped!" );
        // }
    });
})();